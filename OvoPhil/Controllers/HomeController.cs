﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OvoPhil.Models;

namespace OvoPhil.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            var ovoDataModel = new OvoAccount();
            ovoDataModel.bioLine1 = "Sc: philroy101";
            ovoDataModel.bioLine2 = "UWI pelican 📚";
            ovoDataModel.bioLine3 = "BSc (Hons) 🎓";
            ovoDataModel.bioLine4 = "Matthew 22:37📖🙏🏾";
            ovoDataModel.following = 1124;
            ovoDataModel.follwers = 1265;
            ovoDataModel.username = "ovo.phil_";
            ovoDataModel.profileImage = "https://instagram.fkin1-1.fna.fbcdn.net/v/t51.2885-19/s150x150/101069687_335901340728974_2049496554712072192_n.jpg?_nc_ht=instagram.fkin1-1.fna.fbcdn.net&_nc_ohc=3m0jT6jypx0AX_T83cv&oh=db56af05ddfd12ae929c973b7fb180d1&oe=5F6B8316";
            ViewData["OvoAccount"] = ovoDataModel;
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
