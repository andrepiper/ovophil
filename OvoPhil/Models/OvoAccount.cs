﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OvoPhil.Models
{
    public class OvoAccount
    {
        public string profileImage { get; set; }
        public string username { get; set; }
        public int follwers { get; set; }
        public int following { get; set; }
        public string bioLine1 { get; set; }
        public string bioLine2 { get; set; }
        public string bioLine3 { get; set; }
        public string bioLine4 { get; set; }

    }
}
